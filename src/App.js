import Provider from "./Context/Provider";
import { MuiPickersUtilsProvider } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import ptLocale from "date-fns/locale/pt-BR";
import RouterImpl from "./Pages";

import "./App.css";

function App() {
  return (
    <Provider>
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={ptLocale}>
        <div className="App">
          <RouterImpl />
        </div>
      </MuiPickersUtilsProvider>
    </Provider>
  );
}

export default App;
