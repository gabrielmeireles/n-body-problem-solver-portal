let API_URL;

if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
  API_URL = "http://localhost:5000";
} else {
  API_URL = "https://n-body-problem-solver.herokuapp.com";
}

export default API_URL;
