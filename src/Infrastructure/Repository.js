import axios from "axios";
import API_URL from "../helper/EnvHelper";

class Repository {
  static getBodies = async (body) => {
    return axios
      .post(`${API_URL}/bodies`, body, {
        headers: { "Content-Type": "application/json" },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return null;
      });
  };

  static simulate = async (body) => {
    return axios
      .post(`${API_URL}/simulate`, body, {
        headers: { "Content-Type": "application/json" },
      })
      .then((response) => {
        return response.data;
      })
      .catch((error) => {
        return null;
      });
  };
}

export default Repository;
