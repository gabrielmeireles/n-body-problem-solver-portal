import Repository from "./Repository";

class Service {
  static getBodies = (simulationParameters) => {
    const body = {
      BodyIds: simulationParameters.knownBodies.map((body) => body.value),
      SimulationDate: simulationParameters.simulationStartDate.getTime(),
    };
    return Repository.getBodies(body);
  };

  static simulate = (simulationParameters) => {
    const body = {
      Bodies: [
        ...simulationParameters.filledBodies,
        ...simulationParameters.otherBodies.map((body) => ({
          ...body,
          mass: Number(body.mass),
          radius: Number(body.radius),
          currentPosition: {
            x: Number(body.currentPosition.x),
            y: Number(body.currentPosition.y),
            z: Number(body.currentPosition.z),
          },
          currentVelocity: {
            x: Number(body.currentVelocity.x),
            y: Number(body.currentVelocity.y),
            z: Number(body.currentVelocity.z),
          },
        })),
      ],
      ESails: simulationParameters.eSails.map((body) => ({
        ...body,
        mass: Number(body.mass),
        radius: Number(body.wireRadius / 1000),
        currentPosition: {
          x: Number(body.currentPosition.x),
          y: Number(body.currentPosition.y),
          z: Number(body.currentPosition.z),
        },
        currentVelocity: {
          x: Number(body.currentVelocity.x),
          y: Number(body.currentVelocity.y),
          z: Number(body.currentVelocity.z),
        },
        numberWires: Number(body.numberWires),
        wireRadius: Number(body.wireRadius),
        wireTension: Number(body.wireTension),
        wireLength: Number(body.wireLength),
      })),
      SolarSails: simulationParameters.solarSails.map((body) => ({
        ...body,
        mass: Number(body.mass),
        radius: Number(body.radius),
        currentPosition: {
          x: Number(body.currentPosition.x),
          y: Number(body.currentPosition.y),
          z: Number(body.currentPosition.z),
        },
        currentVelocity: {
          x: Number(body.currentVelocity.x),
          y: Number(body.currentVelocity.y),
          z: Number(body.currentVelocity.z),
        },
        theta: Number(body.theta),
        alpha: Number(body.alpha),
        delta: Number(body.delta),
        area: Number(body.area),
        rDiff: Number(body.rDiff),
        rSpec: Number(body.rSpec),
        ef: Number(body.ef),
        eb: Number(body.eb),
        af: Number(body.af),
        ab: Number(body.ab),
        chiF: Number(body.chiF),
        chiB: Number(body.chiB),
      })),
      NumberSteps: Number(simulationParameters.numberSteps),
      SimulationDays: Number(simulationParameters.simulationDays),
    };

    return Repository.simulate(body);
  };

  static generateGraphs = async (simulationResult, simulationDays) => {
    const groupedBodies = [
      ...simulationResult.planets,
      ...simulationResult.eSails,
      ...simulationResult.solarSails,
    ];

    //const earth = groupedBodies.filter((planet) => planet.id === 599)[0];

    const traces = groupedBodies.map((planet) => ({
      // x: planet.bodyTrajectory.x.map(
      //   (value, index) => value - earth.bodyTrajectory.x[index]
      // ),
      // y: planet.bodyTrajectory.y.map(
      //   (value, index) => value - earth.bodyTrajectory.y[index]
      // ),
      // z: planet.bodyTrajectory.z.map(
      //   (value, index) => value - earth.bodyTrajectory.z[index]
      // ),
      x: planet.bodyTrajectory.x,
      y: planet.bodyTrajectory.y,
      z: planet.bodyTrajectory.z,
      text: planet.name,
      name: planet.name,
      type: "scatter",
      mode: "lines",
    }));

    const traces3d = traces.map((trace) => ({
      ...trace,
      type: "scatter3d",
    }));

    const specificEnergySunBodies = groupedBodies.filter(
      (body) => body.specificMechanicalEnergySun
    );
    const specificEnergyEarthBodies = groupedBodies.filter(
      (body) => body.specificMechanicalEnergyEarth
    );

    const steps = groupedBodies[0].bodyTrajectory.x.length;
    const reduction = steps / 5000;

    const X_AXIS = Array.from(Array(steps).keys()).map(
      (item) => (item + 1) / (steps / (simulationDays * reduction))
    );

    const tracesSpecificMechanicalEnergyEarth = specificEnergyEarthBodies.map(
      (body) => ({
        x: X_AXIS,
        y: body.specificMechanicalEnergyEarth,
        text: body.name,
        name: body.name,
        type: "scatter",
        mode: "lines",
      })
    );

    const tracesSpecificMechanicalEnergySun = specificEnergySunBodies.map(
      (body) => ({
        x: X_AXIS,
        y: body.specificMechanicalEnergySun,
        text: body.name,
        name: body.name,
        type: "scatter",
        mode: "lines",
      })
    );

    const tracesPositionX = groupedBodies.map((body) => ({
      x: X_AXIS,
      y: body.bodyTrajectory.x,
      text: body.name,
      name: body.name,
      type: "scatter",
      mode: "lines",
    }));

    const tracesPositionY = groupedBodies.map((body) => ({
      x: X_AXIS,
      y: body.bodyTrajectory.y,
      text: body.name,
      name: body.name,
      type: "scatter",
      mode: "lines",
    }));

    const tracesPositionZ = groupedBodies.map((body) => ({
      x: X_AXIS,
      y: body.bodyTrajectory.z,
      text: body.name,
      name: body.name,
      type: "scatter",
      mode: "lines",
    }));

    return {
      graph2D: traces,
      graph3D: traces3d,
      positionX: tracesPositionX,
      positionY: tracesPositionY,
      positionZ: tracesPositionZ,
      specificEnergyEarth: tracesSpecificMechanicalEnergyEarth,
      specificEnergySun: tracesSpecificMechanicalEnergySun,
      collision: simulationResult.collision,
    };
  };

  static areArraysEqual = (array1, array2) => {
    const arr1 = array1.concat().sort();
    const arr2 = array2.concat().sort();

    for (let i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) {
        return false;
      }
    }

    return true;
  };
}

export default Service;
