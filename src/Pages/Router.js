import React from "react";
import { pages } from "../helper/Consts";
import ConfigurationSelectionImpl from "./ConfigurationsSelection/index";
import LoadingSimulationImpl from "./LoadingSimulation";
import ParametersModalImpl from "./ParametersModal";
import GraphsImpl from "./Graphs";

import "./Router.scss";

const Router = ({ context }) => {
  const { currentPage } = context;

  let CurrentPageComponent = ConfigurationSelectionImpl;

  switch (currentPage) {
    case pages.configurationSelection:
      CurrentPageComponent = ConfigurationSelectionImpl;
      break;
    case pages.loadingPage:
      CurrentPageComponent = LoadingSimulationImpl;
      break;
    case pages.graphsPage:
      CurrentPageComponent = GraphsImpl;
      break;
    default:
      break;
  }

  return (
    <div className="page">
      <CurrentPageComponent />
      <ParametersModalImpl />
    </div>
  );
};

export default Router;
