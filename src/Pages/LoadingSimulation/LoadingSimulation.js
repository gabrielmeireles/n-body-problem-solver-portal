import React from "react";
import Service from "../../Infrastructure/Service";

import Timeline from "@material-ui/lab/Timeline";
import TimelineItem from "@material-ui/lab/TimelineItem";
import TimelineSeparator from "@material-ui/lab/TimelineSeparator";
import TimelineConnector from "@material-ui/lab/TimelineConnector";
import TimelineContent from "@material-ui/lab/TimelineContent";
import TimelineDot from "@material-ui/lab/TimelineDot";

import { makeStyles } from "@material-ui/core/styles";

import { PrimaryButton } from "../../Components/Button";
import { ReactComponent as StepLoading } from "./../../Assets/stepLoading.svg";
import { ReactComponent as StepCompleted } from "./../../Assets/stepCompleted.svg";
import { ReactComponent as StepNotStarted } from "./../../Assets/stepNotStarted.svg";
import { ReactComponent as StepError } from "./../../Assets/stepError.svg";

import "./LoadingSimulation.scss";
import { pages } from "../../helper/Consts";

const useStyles = makeStyles((theme) => ({
  paper: {
    padding: "6px 16px",
  },
  secondaryTail: {
    backgroundColor: theme.palette.secondary.main,
  },
  connector: {
    height: "100px",
    width: "3px",
  },
  dot: {
    border: "none",
    background: "unset",
    boxShadow: "none",
  },
}));

const LoadingSimulation = ({ context }) => {
  const {
    simulationParameters,
    setSimulationParameters,
    loadingOptions,
    setLoadingOptions,
    setGraphsData,
    setCurrentPage,
    modalOptions,
    setModalOptions,
  } = context;

  const setStartLoad = (field) => {
    setLoadingOptions({
      ...loadingOptions,
      [field]: {
        ...loadingOptions[field],
        loading: true,
        error: false,
        success: false,
      },
    });
  };

  const setSucceedLoad = (field) => {
    setLoadingOptions({
      ...loadingOptions,
      [field]: {
        ...loadingOptions[field],
        loading: false,
        success: true,
        error: false,
      },
    });
  };

  const setErrorLoad = (field) => {
    setLoadingOptions({
      ...loadingOptions,
      [field]: {
        ...loadingOptions[field],
        loading: false,
        success: false,
        error: true,
      },
    });
  };

  const cleanErrors = () => {
    setLoadingOptions({
      ...loadingOptions,
      searchingEphemerities: {
        ...loadingOptions.searchingEphemerities,
        error: false,
      },
      calculatingTrajectories: {
        ...loadingOptions.calculatingTrajectories,
        error: false,
      },
      graphsReady: {
        ...loadingOptions.graphsReady,
        error: false,
      },
    });
  };

  const loadBodies = async () => {
    setStartLoad("searchingEphemerities");
    const bodies = await Service.getBodies(simulationParameters);

    if (bodies) {
      await setSimulationParameters({
        ...simulationParameters,
        filledBodies: bodies,
      });

      setSucceedLoad("searchingEphemerities");
    } else {
      setErrorLoad("searchingEphemerities");
    }
  };

  const loadSimulate = async () => {
    setStartLoad("calculatingTrajectories");
    const result = await Service.simulate(simulationParameters);

    if (result) {
      await setSimulationParameters({
        ...simulationParameters,
        simulationResult: result,
      });

      setSucceedLoad("calculatingTrajectories");
    } else {
      setErrorLoad("calculatingTrajectories");
    }
  };

  const loadGraphs = async () => {
    setStartLoad("graphsReady");

    const graphsData = await Service.generateGraphs(
      simulationParameters.simulationResult,
      simulationParameters.simulationDays
    );
    await setGraphsData(graphsData);

    setSucceedLoad("graphsReady");
  };

  const loadStep = () => {
    const {
      calculatingTrajectories,
      searchingEphemerities,
      graphsReady,
    } = loadingOptions;

    if (
      !searchingEphemerities.success &&
      !searchingEphemerities.loading &&
      !searchingEphemerities.error
    ) {
      loadBodies();
    } else if (
      searchingEphemerities.success &&
      !calculatingTrajectories.error &&
      !calculatingTrajectories.success &&
      !calculatingTrajectories.loading
    ) {
      loadSimulate();
    } else if (
      calculatingTrajectories.success &&
      !graphsReady.success &&
      !graphsReady.loading
    ) {
      loadGraphs();
    }
  };

  React.useEffect(() => {
    loadStep();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loadingOptions]);

  const classes = useStyles();
  const timelineStep = (item, isLast) => {
    let StatusIcon = StepNotStarted;

    if (item.success) {
      StatusIcon = StepCompleted;
    } else if (item.error) {
      StatusIcon = StepError;
    } else if (item.loading) {
      StatusIcon = StepLoading;
    }

    return (
      <TimelineItem>
        <TimelineSeparator>
          <TimelineDot className={classes.dot}>
            <StatusIcon />
          </TimelineDot>
          {!isLast ? <TimelineConnector className={classes.connector} /> : null}
        </TimelineSeparator>
        <TimelineContent>
          <span className="fontSubtitle">{item.text}</span>
        </TimelineContent>
      </TimelineItem>
    );
  };

  return (
    <div className="loadingSimulationPage">
      <h1>CARREGANDO SIMULAÇÃO</h1>
      <Timeline align="alternate">
        {timelineStep(loadingOptions.searchingEphemerities)}
        {timelineStep(loadingOptions.calculatingTrajectories)}
        {timelineStep(loadingOptions.graphsReady, true)}
      </Timeline>
      <div className="loadingSimulationPage-buttons">
        <div>
          <PrimaryButton
            onClick={() => setModalOptions({ ...modalOptions, open: true })}
            text="Mudar Parâmetros"
          />
        </div>
        <div>
          <PrimaryButton
            onClick={() => cleanErrors()}
            text="Tentar Novamente"
            disabled={
              !loadingOptions.searchingEphemerities.error &&
              !loadingOptions.calculatingTrajectories.error &&
              !loadingOptions.graphsReady.error
            }
          />
          <div className="background" />
        </div>
        <div>
          <PrimaryButton
            onClick={() => setCurrentPage(pages.graphsPage)}
            text="Visualizar Gráficos"
            disabled={!loadingOptions.graphsReady.success}
          />
          <div className="background" />
        </div>
      </div>
    </div>
  );
};

export default LoadingSimulation;
