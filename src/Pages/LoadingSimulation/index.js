import Context from "../../Context/Context";
import LoadingSimulation from "./LoadingSimulation";

const LoadingSimulationImpl = () => (
  <Context.Consumer>
    {(context) => <LoadingSimulation context={context} />}
  </Context.Consumer>
);

export default LoadingSimulationImpl;
