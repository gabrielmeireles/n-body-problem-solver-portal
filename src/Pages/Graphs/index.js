import Graphs from "./Graphs";
import Context from "../../Context/Context";

const GraphsImpl = () => (
  <Context.Consumer>
    {(context) => <Graphs context={context} />}
  </Context.Consumer>
);

export default GraphsImpl;
