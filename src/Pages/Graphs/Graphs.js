import React from "react";
import Plot from "react-plotly.js";
import { SecondaryButton } from "../../Components/Button";
import { pages } from "../../helper/Consts";
import { graphItems } from "./graphData";
import { ReactComponent as AlertIcon } from "../../Assets/alert.svg";

import "./Graphs.scss";

const Graphs = ({ context }) => {
  const { graphsData, setCurrentPage, setModalOptions, modalOptions } = context;
  const [currentGraph, setCurrentGraph] = React.useState({
    name: "Trajetórias 2D",
    data: "graph2D",
  });

  const renderSidebar = () => (
    <div className="graphsPage-sidebar">
      <div className="graphsPage-sidebarItemsList">
        {graphItems.map((item) => (
          <div className="graphsPage-sidebarItem">
            <div className="graphsPage-sidebarItemInfo">
              {item.icon}
              <h2>{item.name}</h2>
            </div>
            <div className="graphsPage-sidebarSubitems">
              {item.subitems.map((subitem) => (
                <span
                  className={`fontText ${
                    subitem.name === currentGraph.name ? "active" : ""
                  }`}
                  onClick={() => setCurrentGraph(subitem)}
                >
                  {subitem.name}
                </span>
              ))}
            </div>
          </div>
        ))}
      </div>
      <div className="graphsPage-sidebarButtons">
        <SecondaryButton
          onClick={() => {
            setCurrentPage(pages.configurationSelection);
          }}
          text="TELA INICIAL"
        />
        <SecondaryButton
          onClick={() => {
            setModalOptions({
              ...modalOptions,
              open: true,
            });
          }}
          text="MUDAR PARÂMETROS"
        />
      </div>
    </div>
  );

  return (
    <div className="graphsPage">
      {renderSidebar()}
      <div className="graphsPage-plotWrapper">
        <Plot
          data={graphsData[currentGraph.data]}
          layout={{
            width: 1000,
            height: 700,
            title: currentGraph.name,
            hovermode: "closest",
            xaxis: {
              title: {
                text: currentGraph.xaxis,
              },
            },
            yaxis: {
              title: {
                text: currentGraph.yaxis,
              },
            },
          }}
        />
        {graphsData.collision ? (
          <div className="graphsPage-collision">
            <AlertIcon />
            <span className="fontSubtitle">{graphsData.collision}</span>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default Graphs;
