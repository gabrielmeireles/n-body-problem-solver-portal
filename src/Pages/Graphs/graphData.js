import { ReactComponent as EnergyIcon } from "./../../Assets/energyIcon.svg";
import { ReactComponent as LineIcon } from "./../../Assets/lineIcon.svg";
import { ReactComponent as OrbitIcon } from "./../../Assets/orbitIcon.svg";

export const graphItems = [
  {
    name: "Trajetórias",
    icon: <OrbitIcon />,
    subitems: [
      {
        name: "Trajetórias 2D",
        data: "graph2D",
      },
      {
        name: "Trajetórias 3D",
        data: "graph3D",
      },
    ],
  },
  {
    name: "Posições",
    icon: <LineIcon />,
    subitems: [
      {
        name: "Posição X",
        data: "positionX",
        xaxis: "Tempo (dias)",
      },
      {
        name: "Posição Y",
        data: "positionY",
        xaxis: "Tempo (dias)",
      },
      {
        name: "Posição Z",
        data: "positionZ",
        xaxis: "Tempo (dias)",
      },
    ],
  },
  {
    name: "Energia",
    icon: <EnergyIcon />,
    subitems: [
      {
        name: "Relação ao Sol",
        data: "specificEnergySun",
        xaxis: "Tempo (dias)",
      },
      {
        name: "Relação à Terra",
        data: "specificEnergyEarth",
        xaxis: "Tempo (dias)",
      },
    ],
  },
];
