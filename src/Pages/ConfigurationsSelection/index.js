import Context from "../../Context/Context";
import ConfigurationSelection from "./ConfigurationSelection";

const ConfigurationSelectionImpl = () => (
  <Context.Consumer>
    {(context) => <ConfigurationSelection context={context} />}
  </Context.Consumer>
);

export default ConfigurationSelectionImpl;
