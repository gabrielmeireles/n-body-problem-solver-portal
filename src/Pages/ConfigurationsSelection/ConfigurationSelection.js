import ConfigurationCard from "./Components/ConfigurationCard";
import BackgroundSolarSystem from "../../Images/solar_system.jpeg";
import BackgroundSail from "../../Images/solar_sail.jpeg";
import BackgroundMargareth from "../../Images/marg_thomas.jpg";
import { simulationSails, simulationSolarSystem } from "./defaultsSimulations";
import { pages } from "../../helper/Consts";

import "./ConfigurationSelection.scss";

const cards = [
  {
    title: "Sistema Solar",
    items: [
      "Corpos na simulação: Sol, Mercúrio, Vênus, Terra, Lua, Marte, Júpiter, Saturno, Urano e Netuno.",
      "365 dias de simulação.",
      "100 mil iterações.",
      "Mude os parâmetros da simulação depois.",
    ],
    backgroundImage: BackgroundSolarSystem,
    buttonText: "Simular Agora",
    onClick: async ({ setSimulationParameters, setCurrentPage }) => {
      await setSimulationParameters(simulationSolarSystem());
      setCurrentPage(pages.loadingPage);
    },
  },
  {
    title: "Velas",
    items: [
      "Corpos na simulação: Sol, Terra, Lua, Vela Elétrica, Vela Solar.",
      "550 dias de simulação.",
      "200 mil iterações.",
      "Mude os parâmetros da simulação depois.",
    ],
    backgroundImage: BackgroundSail,
    buttonText: "Simular Agora",
    onClick: async ({ setSimulationParameters, setCurrentPage }) => {
      await setSimulationParameters(simulationSails());
      setCurrentPage(pages.loadingPage);
    },
  },
  {
    title: "Faça Você Mesmo",
    items: [
      "Escolha os corpos do sistema solar que quer simular. Adicione quantas velas e corpos quiser.",
      "Simule quantos dias quiser!",
      "Até 500 mil iterações",
    ],
    backgroundImage: BackgroundMargareth,
    buttonText: "Escolher parâmetros",
    onClick: ({ modalOptions, setModalOptions }) =>
      setModalOptions({ ...modalOptions, open: true }),
  },
];

const ConfigurationSelection = ({ context }) => {
  const {
    modalOptions,
    setModalOptions,
    setSimulationParameters,
    setCurrentPage,
  } = context;
  return (
    <article className="configurationSelection">
      <h1 className="configurationSelection-title">
        Selecione as configurações que deseja usar
      </h1>
      <section className="configurationSelection-cards">
        {cards.map((card) => {
          const { title, items, backgroundImage, buttonText, onClick } = card;
          return (
            <ConfigurationCard
              title={title}
              items={items}
              backgroundImage={backgroundImage}
              buttonText={buttonText}
              onClick={() =>
                onClick({
                  modalOptions,
                  setModalOptions,
                  setSimulationParameters,
                  setCurrentPage,
                })
              }
            />
          );
        })}
      </section>
    </article>
  );
};

export default ConfigurationSelection;
