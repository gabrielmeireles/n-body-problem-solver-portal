import { Button } from "@material-ui/core";
import "./ConfigurationCard.scss";

const ConfigurationCard = ({
  title,
  items,
  backgroundImage,
  buttonText,
  onClick,
}) => {
  return (
    <div
      className="configurationCard"
      style={{
        backgroundImage: `linear-gradient(0deg, rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.4)), url(${backgroundImage})`,
      }}
    >
      <h2 className="configurationCard-title">{title}</h2>
      <ul className="configurationCard-list">
        {items.map((item) => (
          <li className="configurationCard-listItem">{item}</li>
        ))}
      </ul>
      <Button
        className="configurationCard-button"
        onClick={onClick}
        variant="contained"
        color="primary"
      >
        {buttonText}
      </Button>
    </div>
  );
};

export default ConfigurationCard;
