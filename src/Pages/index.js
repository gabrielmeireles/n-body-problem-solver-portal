import Context from "../Context/Context";
import Router from "./Router";

const RouterImpl = () => (
  <Context.Consumer>
    {(context) => <Router context={context} />}
  </Context.Consumer>
);

export default RouterImpl;
