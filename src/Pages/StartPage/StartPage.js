import "./StartPage.css";

const StartPage = ({ setCurrentPage }) => {
  return (
    <article className="startPage">
      <div>
        <h3>Universidade Federal de Minas Gerais</h3>
        <h4>Departamento de Engenharia Mecânica</h4>
      </div>
      <h1>
        Simulação Orbital de E-Sail e Vela Solar com Modelagem de Ordem Superior
      </h1>
      <div className="startPage-developedBy">
        <span>Desenvolvido por: Gabriel Felipe Ribeiro Meireles</span>
        <span>Orientado por: Maria Cecília Pereira Faria</span>
        <button
          type="button"
          className="startPage-simulationButton"
          onClick={() => setCurrentPage(1)}
        >
          Iniciar Simulação
        </button>
      </div>
    </article>
  );
};

export default StartPage;
