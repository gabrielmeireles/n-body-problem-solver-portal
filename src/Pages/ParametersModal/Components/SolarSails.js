import React from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import InputNumber from "../../../Components/InputNumber";
import { SecondaryButton } from "../../../Components/Button";
import AccordionSummaryImpl from "../../../Components/AccordionSummaryImpl";
import { lastItemOfArray } from "../../../helper/Util";

const SolarSails = ({ context }) => {
  const { simulationParameters, setSimulationParameters } = context;

  const setSolarSails = (newSolarSails) => {
    setSimulationParameters({
      ...simulationParameters,
      solarSails: newSolarSails,
    });
  };

  const solarSails = simulationParameters.solarSails;

  const setSailValue = (modifiedSailId, field, newValue) => {
    const newSolarSails = [...solarSails].map((sail) => {
      if (sail.id === modifiedSailId) {
        return {
          ...sail,
          [field]: newValue,
        };
      } else {
        return sail;
      }
    });

    setSolarSails(newSolarSails);
  };

  const renderSailForm = (sail) => (
    <form noValidate autoComplete="off">
      <div>
        <InputNumber
          label="Massa"
          value={sail.mass}
          onChange={(value) => setSailValue(sail.id, "mass", value)}
          adornmentLabel="Kg"
        />
        <InputNumber
          label="Raio"
          value={sail.radius}
          onChange={(value) => setSailValue(sail.id, "radius", value)}
          adornmentLabel="m"
        />
        <InputNumber
          label="Area"
          value={sail.area}
          onChange={(value) => setSailValue(sail.id, "area", value)}
          adornmentLabel="m²"
        />
      </div>
      <div>
        <InputNumber
          label="Posição X"
          value={sail.currentPosition.x}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              x: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Y"
          value={sail.currentPosition.y}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              y: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Z"
          value={sail.currentPosition.z}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              z: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
      </div>
      <div>
        <InputNumber
          label="Velocidade VX"
          value={sail.currentVelocity.x}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              x: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VY"
          value={sail.currentVelocity.y}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              y: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VZ"
          value={sail.currentVelocity.z}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              z: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
      </div>
      <div>
        <InputNumber
          label="Theta"
          value={sail.theta}
          onChange={(value) => setSailValue(sail.id, "theta", value)}
          adornmentLabel="deg"
        />
        <InputNumber
          label="Alpha"
          value={sail.alpha}
          onChange={(value) => setSailValue(sail.id, "alpha", value)}
          adornmentLabel="deg"
        />
        <InputNumber
          label="Delta"
          value={sail.delta}
          onChange={(value) => setSailValue(sail.id, "delta", value)}
          adornmentLabel="deg"
        />
      </div>
      <div>
        <InputNumber
          label="RDiff"
          value={sail.rDiff}
          onChange={(value) => setSailValue(sail.id, "rDiff", value)}
        />
        <InputNumber
          label="RSpec"
          value={sail.rSpec}
          onChange={(value) => setSailValue(sail.id, "rSpec", value)}
        />
        <InputNumber
          label="ChiF"
          value={sail.chiF}
          onChange={(value) => setSailValue(sail.id, "chiF", value)}
        />
        <InputNumber
          label="ChiB"
          value={sail.chiB}
          onChange={(value) => setSailValue(sail.id, "chiB", value)}
        />
      </div>
      <div>
        <InputNumber
          label="ef"
          value={sail.ef}
          onChange={(value) => setSailValue(sail.id, "ef", value)}
        />
        <InputNumber
          label="eb"
          value={sail.eb}
          onChange={(value) => setSailValue(sail.id, "eb", value)}
        />
        <InputNumber
          label="af"
          value={sail.af}
          onChange={(value) => setSailValue(sail.id, "af", value)}
        />
        <InputNumber
          label="ab"
          value={sail.ab}
          onChange={(value) => setSailValue(sail.id, "ab", value)}
        />
      </div>
    </form>
  );

  const renderAccordions = () =>
    solarSails.map((solarSail, index) => (
      <Accordion key={index}>
        <AccordionSummaryImpl
          name={solarSail.name}
          onCopy={() => copySail(solarSail)}
          onDelete={() => deleteSail(solarSail)}
        />
        <AccordionDetails>{renderSailForm(solarSail)}</AccordionDetails>
      </Accordion>
    ));

  const copySail = (sail) => {
    const idNumber = lastItemOfArray(solarSails).idNumber + 1;
    const id = `SS${idNumber}`;
    const newSail = {
      ...sail,
      idNumber: idNumber,
      id: id,
      name: `Vela Solar ${id}`,
    };
    setSolarSails([...solarSails, newSail]);
  };

  const deleteSail = (sail) => {
    const sailsCopy = solarSails.filter((item) => item.id !== sail.id);
    setSolarSails(sailsCopy);
  };

  const addNewSolarSail = () => {
    const idNumber =
      solarSails.length === 0 ? 1 : lastItemOfArray(solarSails).idNumber + 1;
    const id = `SS${idNumber}`;
    const solarSail = {
      idNumber: idNumber,
      id: id,
      name: `Vela Solar ${id}`,
      mass: 100,
      radius: 42000,
      currentPosition: {
        x: 4.5e6,
        y: 0,
        z: 0,
      },
      currentVelocity: {
        x: 0,
        y: 0,
        z: 0,
      },
      referenceBodyId: "399",
      theta: 0,
      alpha: 3,
      delta: 0,
      area: 3795,
      rDiff: 0.0528,
      rSpec: 0.8272,
      ef: 0.05,
      eb: 0.55,
      af: 0.12,
      ab: 0.12,
      chiF: 0.79,
      chiB: 0.55,
    };

    setSolarSails([...solarSails, solarSail]);
  };

  return (
    <div className="sails-wrapper">
      <SecondaryButton onClick={addNewSolarSail} text="Adicionar Vela Solar" />
      {renderAccordions()}
    </div>
  );
};

export default SolarSails;
