import { makeStyles } from "@material-ui/core";
import { DateTimePicker } from "@material-ui/pickers";
import React from "react";
import Select from "react-select";
import InputNumber from "../../../Components/InputNumber";
import "./ModalPages.scss";

const options = [
  { value: 0, label: "Sol" },
  { value: 199, label: "Mercúrio" },
  { value: 299, label: "Vênus" },
  { value: 301, label: "Lua" },
  { value: 399, label: "Terra" },
  { value: 499, label: "Marte" },
  { value: 599, label: "Júpiter" },
  { value: 699, label: "Saturno" },
  { value: 799, label: "Urano" },
  { value: 899, label: "Netuno" },
  { value: 999, label: "Plutão" },
];

const useStyles = makeStyles((theme) => ({
  root: {
    margin: "0",
    width: "250px",
  },
}));

const KnownBodies = ({ context }) => {
  const { simulationParameters, setSimulationParameters } = context;

  const knownBodiesList = simulationParameters.knownBodies;
  const selectedDate = simulationParameters.simulationStartDate;

  const setSimulationParametersField = (field, value) => {
    setSimulationParameters({
      ...simulationParameters,
      [field]: value,
    });
  };

  const classes = useStyles();

  return (
    <div className="bodiesParameters">
      <div className="knownBodies">
        <div className="knownBodies-bodies">
          <span className="knownBodies-chooseBodies">
            Escolha os corpos que deseja simular:
          </span>
          <Select
            options={options}
            value={knownBodiesList}
            onChange={(value) =>
              setSimulationParametersField("knownBodies", value)
            }
            closeMenuOnSelect={false}
            isMulti
            className="knownBodies-selector"
          />
          <span className="knownBodies-chooseBodiesObservation">
            * A quantidade de corpos selecionada influencia exponencialmente no
            tempo de simulação. Caso escolha uma quantidade elevada de corpos,
            para uma simulação mais rápida, sugere-se diminuir o número de
            iterações.
          </span>
        </div>
        <div className="knownBodies-date">
          <span className="knownBodies-chooseDate">
            Selecione a data inicial da simulação
          </span>
          <DateTimePicker
            variant="dialog"
            label="Data da Simulação"
            value={selectedDate}
            onChange={(value) =>
              setSimulationParametersField("simulationStartDate", value)
            }
            format="dd/MM/yyyy HH:mm"
            showTodayButton
            ampm={false}
            todayLabel="HOJE"
          />
        </div>
      </div>
      <div className="parameters">
        <InputNumber
          label="Número de Dias da Simulação"
          value={simulationParameters.simulationDays}
          onChange={(value) =>
            setSimulationParametersField("simulationDays", value)
          }
          classes={classes}
        />
        <InputNumber
          label="Número de Iterações"
          value={simulationParameters.numberSteps}
          onChange={(value) =>
            setSimulationParametersField("numberSteps", value)
          }
          classes={classes}
        />
      </div>
    </div>
  );
};

export default KnownBodies;
