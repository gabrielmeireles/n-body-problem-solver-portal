import React from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import InputNumber from "../../../Components/InputNumber";
import { SecondaryButton } from "../../../Components/Button";
import { lastItemOfArray } from "../../../helper/Util";
import AccordionSummaryImpl from "../../../Components/AccordionSummaryImpl";

const OtherBodies = ({ context }) => {
  const { simulationParameters, setSimulationParameters } = context;

  const setOtherBodies = (newBodies) => {
    setSimulationParameters({
      ...simulationParameters,
      otherBodies: newBodies,
    });
  };

  const otherBodies = simulationParameters.otherBodies;

  const setBodyValue = (modifiedSailId, field, newValue) => {
    const newBody = [...otherBodies].map((body) => {
      if (body.id === modifiedSailId) {
        return {
          ...body,
          [field]: newValue,
        };
      } else {
        return body;
      }
    });

    setOtherBodies(newBody);
  };

  const renderBodyForm = (body) => (
    <form noValidate autoComplete="off">
      <div>
        <InputNumber
          label="Massa"
          value={body.mass}
          onChange={(value) => setBodyValue(body.id, "mass", value)}
          adornmentLabel="Kg"
        />
      </div>
      <div>
        <InputNumber
          label="Posição X"
          value={body.currentPosition.x}
          onChange={(value) => {
            const newPosition = {
              ...body.currentPosition,
              x: value,
            };
            setBodyValue(body.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Y"
          value={body.currentPosition.y}
          onChange={(value) => {
            const newPosition = {
              ...body.currentPosition,
              y: value,
            };
            setBodyValue(body.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Z"
          value={body.currentPosition.z}
          onChange={(value) => {
            const newPosition = {
              ...body.currentPosition,
              z: value,
            };
            setBodyValue(body.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
      </div>
      <div>
        <InputNumber
          label="Velocidade VX"
          value={body.currentVelocity.x}
          onChange={(value) => {
            const newVelocity = {
              ...body.currentVelocity,
              x: value,
            };
            setBodyValue(body.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VY"
          value={body.currentVelocity.y}
          onChange={(value) => {
            const newVelocity = {
              ...body.currentVelocity,
              y: value,
            };
            setBodyValue(body.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VZ"
          value={body.currentVelocity.z}
          onChange={(value) => {
            const newVelocity = {
              ...body.currentVelocity,
              z: value,
            };
            setBodyValue(body.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
      </div>
    </form>
  );

  const renderAccordions = () =>
    otherBodies.map((body, index) => (
      <Accordion key={index}>
        <AccordionSummaryImpl
          name={body.name}
          onCopy={() => copyBody(body)}
          onDelete={() => deleteBody(body)}
        />
        <AccordionDetails>{renderBodyForm(body)}</AccordionDetails>
      </Accordion>
    ));

  const copyBody = (sail) => {
    const idNumber = lastItemOfArray(otherBodies).idNumber + 1;
    const id = `B${idNumber}`;
    const newSail = {
      ...sail,
      idNumber: idNumber,
      id: id,
      name: `Corpo ${id}`,
    };
    setOtherBodies([...otherBodies, newSail]);
  };

  const deleteBody = (sail) => {
    const sailsCopy = otherBodies.filter((item) => item.id !== sail.id);
    setOtherBodies(sailsCopy);
  };

  const addNewBody = () => {
    const idNumber =
      otherBodies.length === 0 ? 1 : lastItemOfArray(otherBodies).idNumber + 1;
    const id = `B${idNumber}`;
    const body = {
      idNumber: idNumber,
      id: id,
      name: `Corpo ${id}`,
      mass: 100,
      radius: 20000,
      referenceBodyId: "0",
      currentPosition: {
        x: 1.496e8,
        y: 0,
        z: 0,
      },
      currentVelocity: {
        x: 0,
        y: 29.78,
        z: 0,
      },
    };

    setOtherBodies([...otherBodies, body]);
  };

  return (
    <div className="sails-wrapper">
      <SecondaryButton onClick={addNewBody} text="Adicionar Novo Corpo" />
      {renderAccordions()}
    </div>
  );
};

export default OtherBodies;
