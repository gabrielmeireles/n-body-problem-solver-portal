import React from "react";
import Accordion from "@material-ui/core/Accordion";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import InputNumber from "../../../Components/InputNumber";
import { SecondaryButton } from "../../../Components/Button";
import { lastItemOfArray } from "../../../helper/Util";
import AccordionSummaryImpl from "../../../Components/AccordionSummaryImpl";

const ESails = ({ context }) => {
  const { simulationParameters, setSimulationParameters } = context;

  const setESails = (newESails) => {
    setSimulationParameters({
      ...simulationParameters,
      eSails: newESails,
    });
  };

  const eSails = simulationParameters.eSails;

  const setSailValue = (modifiedSailId, field, newValue) => {
    const newESails = [...eSails].map((sail) => {
      if (sail.id === modifiedSailId) {
        return {
          ...sail,
          [field]: newValue,
        };
      } else {
        return sail;
      }
    });

    setESails(newESails);
  };

  const renderSailForm = (sail) => (
    <form noValidate autoComplete="off">
      <div>
        <InputNumber
          label="Massa"
          value={sail.mass}
          onChange={(value) => setSailValue(sail.id, "mass", value)}
          adornmentLabel="Kg"
        />
      </div>
      <div>
        <InputNumber
          label="Posição X"
          value={sail.currentPosition.x}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              x: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Y"
          value={sail.currentPosition.y}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              y: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
        <InputNumber
          label="Posição Z"
          value={sail.currentPosition.z}
          onChange={(value) => {
            const newPosition = {
              ...sail.currentPosition,
              z: value,
            };
            setSailValue(sail.id, "currentPosition", newPosition);
          }}
          adornmentLabel="Km"
        />
      </div>
      <div>
        <InputNumber
          label="Velocidade VX"
          value={sail.currentVelocity.x}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              x: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VY"
          value={sail.currentVelocity.y}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              y: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
        <InputNumber
          label="Velocidade VZ"
          value={sail.currentVelocity.z}
          onChange={(value) => {
            const newVelocity = {
              ...sail.currentVelocity,
              z: value,
            };
            setSailValue(sail.id, "currentVelocity", newVelocity);
          }}
          adornmentLabel="Km/s"
        />
      </div>
      <div>
        <InputNumber
          label="Número de Fios"
          value={sail.numberWires}
          onChange={(value) => setSailValue(sail.id, "numberWires", value)}
        />
        <InputNumber
          label="Raio do Fio"
          value={sail.wireRadius}
          onChange={(value) => setSailValue(sail.id, "wireRadius", value)}
          adornmentLabel="m"
        />
        <InputNumber
          label="Tensão do Fio"
          value={sail.wireTension}
          onChange={(value) => setSailValue(sail.id, "wireTension", value)}
          adornmentLabel="V"
        />
        <InputNumber
          label="Comprimento do Fio"
          value={sail.wireLength}
          onChange={(value) => setSailValue(sail.id, "wireLength", value)}
          adornmentLabel="m"
        />
      </div>
    </form>
  );

  const renderAccordions = () =>
    eSails.map((eSail, index) => (
      <Accordion key={index}>
        <AccordionSummaryImpl
          name={eSail.name}
          onCopy={() => copySail(eSail)}
          onDelete={() => deleteSail(eSail)}
        />
        <AccordionDetails>{renderSailForm(eSail)}</AccordionDetails>
      </Accordion>
    ));

  const copySail = (sail) => {
    const idNumber = lastItemOfArray(eSails).idNumber + 1;
    const id = `ES${idNumber}`;
    const newSail = {
      ...sail,
      idNumber: idNumber,
      id: id,
      name: `E-Sail ${id}`,
    };
    setESails([...eSails, newSail]);
  };

  const deleteSail = (sail) => {
    const sailsCopy = eSails.filter((item) => item.id !== sail.id);
    setESails(sailsCopy);
  };

  const addNewESail = () => {
    const idNumber =
      eSails.length === 0 ? 1 : lastItemOfArray(eSails).idNumber + 1;
    const id = `ES${idNumber}`;
    const eSail = {
      idNumber: idNumber,
      id: id,
      name: `E-Sail ${id}`,
      mass: 100,
      currentPosition: {
        x: 4.5e6,
        y: 0,
        z: 0,
      },
      currentVelocity: {
        x: 0,
        y: 0,
        z: 0,
      },
      referenceBodyId: "399",
      numberWires: 100,
      wireRadius: 1e-5,
      wireTension: 12000,
      wireLength: 20000,
      attitude: {
        phi: 0,
        theta: 0,
        psi: 0,
      },
    };

    setESails([...eSails, eSail]);
  };

  return (
    <div className="sails-wrapper">
      <SecondaryButton onClick={addNewESail} text="Adicionar E-Sail" />
      {renderAccordions()}
    </div>
  );
};

export default ESails;
