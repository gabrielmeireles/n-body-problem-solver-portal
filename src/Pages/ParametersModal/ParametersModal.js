import { Modal } from "@material-ui/core";
import { PrimaryButton } from "../../Components/Button";
import ESails from "./Components/ESails";
import KnownBodies from "./Components/KnownBodies";
import SolarSails from "./Components/SolarSails";
import OtherBodies from "./Components/OtherBodies";

import "./ParametersModal.scss";

const ParametersModal = ({ context }) => {
  const { modalOptions, setModalOptions, restartSimulation } = context;

  const goToNextPage = () => {
    setModalOptions({
      ...modalOptions,
      currentPage: modalOptions.currentPage + 1,
    });
  };

  const goToPreviousPage = () => {
    setModalOptions({
      ...modalOptions,
      currentPage: modalOptions.currentPage - 1,
    });
  };

  const isFirstPage = modalOptions.currentPage === 1;
  const isLastPage = modalOptions.currentPage === 4;

  let Content;
  switch (modalOptions.currentPage) {
    case 1:
      Content = KnownBodies;
      break;
    case 2:
      Content = ESails;
      break;
    case 3:
      Content = SolarSails;
      break;
    case 4:
      Content = OtherBodies;
      break;
    default:
      Content = <div />;
  }

  return (
    <Modal
      open={modalOptions.open}
      onClose={() => setModalOptions({ ...modalOptions, open: false })}
      className="modal-wrapper"
    >
      <article className="parametersModal">
        <header>
          <h1>ESCOLHA AS CONFIRAÇÕES QUE DESEJA USAR</h1>
        </header>
        <section className="parametersModal-content">
          <Content context={context} />
        </section>
        <footer className="parametersModal-buttons">
          <PrimaryButton
            text="ANTERIOR"
            onClick={goToPreviousPage}
            disabled={isFirstPage}
          />
          <PrimaryButton
            text="SIMULAR"
            onClick={() => {
              restartSimulation();
            }}
          />
          <PrimaryButton
            text="PRÓXIMO"
            onClick={goToNextPage}
            disabled={isLastPage}
          />
        </footer>
      </article>
    </Modal>
  );
};

export default ParametersModal;
