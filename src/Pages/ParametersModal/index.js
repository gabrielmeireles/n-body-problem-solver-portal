import Context from "../../Context/Context";
import ParametersModal from "./ParametersModal";

const ParametersModalImpl = () => (
  <Context.Consumer>
    {(context) => <ParametersModal context={context} />}
  </Context.Consumer>
);

export default ParametersModalImpl;
