import React, { useState } from "react";
import { pages } from "../helper/Consts";
import Service from "../Infrastructure/Service";
import Context from "./Context";

const INITIAL_MODAL_OPTIONS = {
  open: false,
  currentPage: 1,
};

const INITIAL_SIMULATION_PARAMETERS = {
  knownBodies: [{ value: 0, label: "Sol" }],
  otherBodies: [],
  eSails: [],
  solarSails: [],
  filledBodies: [],
  numberSteps: 200000,
  simulationDays: 365,
  simulationStartDate: new Date(),
  simulationResult: null,
};

const INITIAL_CURRENT_PAGE = pages.configurationSelection;

const INITIAL_LOADING_OPTIONS = {
  searchingEphemerities: {
    success: false,
    error: false,
    loading: false,
    text: "Buscando efemérides no sistema NASA Horizons",
  },
  calculatingTrajectories: {
    success: false,
    error: false,
    loading: false,
    text: "Calculando trajetórias para o problema de N corpos",
  },
  graphsReady: {
    success: false,
    error: false,
    loading: false,
    text: "Simulação concluída e gráficos gerados",
  },
};

const INITIAL_GRAPHS = {
  graph2D: {},
  graph3D: {},
  specificEnergyEarth: {},
  specificEnergySun: {},
};

const Provider = ({ ...props }) => {
  const [currentPage, setCurrentPage] = useState(INITIAL_CURRENT_PAGE);
  const [modalOptions, setModalOptions] = useState(INITIAL_MODAL_OPTIONS);
  const [loadingOptions, setLoadingOptions] = useState(INITIAL_LOADING_OPTIONS);
  const [graphsData, setGraphsData] = useState(INITIAL_GRAPHS);

  const [simulationParameters, setSimulationParametersImpl] = useState(
    INITIAL_SIMULATION_PARAMETERS
  );

  const restartSimulation = async () => {
    setLoadingOptions({
      ...INITIAL_LOADING_OPTIONS,
      searchingEphemerities: loadingOptions.searchingEphemerities,
    });
    setModalOptions({
      ...modalOptions,
      open: false,
    });
    setCurrentPage(pages.loadingPage);
  };

  const setSimulationParameters = (parameters) => {
    const dateEquals =
      parameters.simulationStartDate ===
      simulationParameters.simulationStartDate;
    const bodiesEqual = Service.areArraysEqual(
      parameters.knownBodies.map((body) => body.value),
      simulationParameters.knownBodies.map((body) => body.value)
    );
    if (!dateEquals || !bodiesEqual) {
      setLoadingOptions(INITIAL_LOADING_OPTIONS);
    }
    setSimulationParametersImpl(parameters);
  };

  return (
    <Context.Provider
      value={{
        currentPage,
        setCurrentPage,
        simulationParameters,
        setSimulationParameters,
        modalOptions,
        setModalOptions,
        loadingOptions,
        setLoadingOptions,
        graphsData,
        setGraphsData,
        restartSimulation,
      }}
    >
      {props.children}
    </Context.Provider>
  );
};

export default Provider;
