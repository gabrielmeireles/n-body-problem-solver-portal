import { Button } from "@material-ui/core";

export const PrimaryButton = ({ text, onClick, disabled }) => (
  <Button
    className="base-button primary"
    onClick={onClick}
    variant="contained"
    color="primary"
    disabled={disabled}
  >
    {text}
  </Button>
);

export const SecondaryButton = ({ text, onClick }) => (
  <Button
    className="base-button secondary"
    onClick={onClick}
    variant="contained"
    color="secondary"
  >
    {text}
  </Button>
);

export const OutlinedButton = ({ text, onClick, disabled }) => (
  <Button
    className="base-button outlined"
    onClick={onClick}
    variant="outlined"
    disabled={disabled}
  >
    {text}
  </Button>
);
