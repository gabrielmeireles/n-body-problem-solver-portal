import AccordionSummary from "@material-ui/core/AccordionSummary";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { ReactComponent as DeleteIcon } from "../Assets/delete.svg";
import { ReactComponent as CopyIcon } from "../Assets/contentCopy.svg";
import Tooltip from "@material-ui/core/Tooltip";

const AccordionSummaryImpl = ({ name, onCopy, onDelete }) => (
  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
    <div className="accordionSummary">
      <span>{name}</span>
      <div>
        <Tooltip title="Criar Cópia" placement="top">
          <CopyIcon
            onClick={(e) => {
              onCopy();
              e.stopPropagation();
            }}
          />
        </Tooltip>
        <Tooltip title="Excluir" placement="top">
          <DeleteIcon
            onClick={(e) => {
              onDelete();
              e.stopPropagation();
            }}
            style={{ fill: "red" }}
          />
        </Tooltip>
      </div>
    </div>
  </AccordionSummary>
);

export default AccordionSummaryImpl;
