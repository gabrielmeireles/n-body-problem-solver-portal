import TextField from "@material-ui/core/TextField";
import InputAdornment from "@material-ui/core/InputAdornment";
import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "150px",
  },
}));

const useStylesField = makeStyles((theme) => ({
  root: {
    margin: "12px",
  },
}));

const InputNumber = (props) => {
  const defaultClasses = useStyles();
  const classes = props.classes || defaultClasses;
  const { adornmentLabel, onChange, ...other } = props;
  return (
    <TextField
      {...other}
      onChange={(event) => onChange(event.target.value)}
      classes={useStylesField()}
      type="text"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">{adornmentLabel}</InputAdornment>
        ),
        classes,
      }}
    />
  );
};

export default InputNumber;
